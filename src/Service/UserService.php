<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 29.04.18
 * Time: 10:26
 */

namespace App\Service;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;

class UserService
{

    public function regist($request){
        $user = new User();
        $user->setName($request->get('name'));
        $user->setPassword($request->get('password'));
        return $user;
    }
}
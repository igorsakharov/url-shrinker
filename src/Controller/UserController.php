<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->render('user/user_profile.html.twig', [
            'controller_name' => 'UserController',
            'user_name' => 'insert your name'
        ]);
    }

    public function create(Request $request , UserService $service){
        $manager = $this->getDoctrine()->getManager();
        $user = $service->regist($request);
        $manager->persist($user);
        $manager->flush();
        $repository = $this->getDoctrine()->getRepository(User::class);
        $users = $repository->findAll();
        return $this->render('user/user_list.html.twig',[
            'users'=>$users
        ]);
    }

    public function exist(Request $request){
        $user = $this->getDoctrine()->getRepository(User::class);
        $data = (array)json_decode($request->getContent());
        $db_response = array_values((array)$user->findOneBy($data));
        if ($db_response){
            return new Response(json_encode($db_response));
        }
        return  new Response(json_encode([
            'error'=>'do not work'
        ]));
    }
}
